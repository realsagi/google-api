
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 19, 2013 at 02:39 AM
-- Server version: 5.1.67
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u494259602_table`
--

-- --------------------------------------------------------

--
-- Table structure for table `table_persona2`
--

CREATE TABLE IF NOT EXISTS `table_persona2` (
  `d_number` int(10) NOT NULL AUTO_INCREMENT,
  `d_fname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `d_lname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `d_Class` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `d_Call` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `d_lat` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `d_lag` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `d_sex` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `d_address` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `d_date` date NOT NULL,
  `d_order` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`d_number`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `table_persona2`
--

INSERT INTO `table_persona2` (`d_number`, `d_fname`, `d_lname`, `d_Class`, `d_Call`, `d_lat`, `d_lag`, `d_sex`, `d_address`, `d_date`, `d_order`) VALUES
(15, 'test', 'test', 'อยู่ในน้ำ', '0888888888', '14.904306706305885', '101.9958524169922', 'Male', 'ถนน สุขวิถี 2 ตำบล สุรนารี จังหวัด นครราชสีมา 30000 ประเทศไทย', '2013-10-19', 'อยู่ในน้ำ'),
(14, 'สถาพร', 'โง่นทา', 'aaa', '0858589936', '14.863826688371793', '101.9793729248047', 'Male', 'ถนน วงแหวนรอบเมือง ตำบล โคกกรวด จังหวัด นครราชสีมา 30280 ประเทศไทย', '2013-10-19', ''),
(13, 'สถาพร', 'โง่นทา', 'aaa', '', '14.886058458892135', '101.99482244873047', 'Male', 'ถนน สุขวิถี 2 ตำบล สุรนารี จังหวัด นครราชสีมา 30000 ประเทศไทย', '2013-10-19', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
